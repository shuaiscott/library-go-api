package v1

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestReturnAllBooks(t *testing.T) {
	// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
	// pass 'nil' as the third parameter.
	req, err := http.NewRequest("GET", "/v1/books", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(ReturnAllBooks)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := "[{\"id\":\"1\",\"title\":\"Harry Potter and the Sorcerer's Stone\",\"author\":\"J. K. Rowling\",\"ISBN\":\"9780439362139\",\"publisher\":\"Arthur A. Levine Books\",\"published_date\":\"November 1, 2001\"},{\"id\":\"2\",\"title\":\"Harry Potter and Chamber of Secrets\",\"author\":\"J. K. Rowling\",\"ISBN\":\"9780439064873\",\"publisher\":\"Scholastic Inc.\",\"published_date\":\"September 1, 2000\"}]\n"
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body \n\tgot: %+v\n\twant: %+v",
			rr.Body.String(), expected)
	}
}