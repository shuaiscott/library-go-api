package v1

import (
    "fmt"
    "net/http"
    "github.com/gorilla/mux"
    // "database/sql"
    // "github.com/lib/pq"
    "encoding/json"
    "io/ioutil"
)

// Book : A library book structure
type Book struct {
    ID string `json:"id"`
    Title string `json:"title"`
    Author string `json:"author"`
    ISBN string `json:"ISBN"`
    Publisher string `json:"publisher"`
    PublishedDate string `json:"published_date"`
}

var books = []Book{
    Book{ID: "1", Title: "Harry Potter and the Sorcerer's Stone", Author: "J. K. Rowling", ISBN: "9780439362139", Publisher: "Arthur A. Levine Books", PublishedDate: "November 1, 2001"},
    Book{ID: "2", Title: "Harry Potter and Chamber of Secrets", Author: "J. K. Rowling", ISBN: "9780439064873", Publisher: "Scholastic Inc.", PublishedDate: "September 1, 2000"},
}

// ReturnAllBooks : Returns all books in the library
func ReturnAllBooks(w http.ResponseWriter, r *http.Request){
    fmt.Println("Endpoint Hit: v1ReturnAllBooks")

    json.NewEncoder(w).Encode(books)
}

// ReturnSingleBook : Returns a single book by id value
func ReturnSingleBook(w http.ResponseWriter, r *http.Request){
	fmt.Println("Endpoint Hit: v1ReturnSingleBook")
    vars := mux.Vars(r)
    id := vars["id"]

    found := false

    for _, book := range books {
        if book.ID == id {
            json.NewEncoder(w).Encode(book)
            found = true
            break
        }
    }
    // handling when book not found
    if found == false {
        http.Error(w, "Not Found", http.StatusNotFound)
    }
}

// CreateBook : Creates a book in the library
func CreateBook(w http.ResponseWriter, r *http.Request) {   
	fmt.Println("Endpoint Hit: v1CreateBook")
    reqBody, _ := ioutil.ReadAll(r.Body)
    
    var book Book 
    json.Unmarshal(reqBody, &book)

    // TODO Add handling when book format/data is missing

    books = append(books, book)

    json.NewEncoder(w).Encode(book)
}

// DeleteBook : Deletes a book from the library by an id
func DeleteBook(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: v1DeleteBook")
	vars := mux.Vars(r)

	id := vars["id"]

    found := false

	for index, book := range books {
		if book.ID == id {
			books = append(books[:index], books[index+1:]...)
            found = true
            break
		}
	}
    
    // handling when book not found
    if found == false {
        http.Error(w, "Not Found", http.StatusNotFound)
    }
}

// UpdateBook : Updates a book by id
func UpdateBook(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: v1UpdateBook")
	vars := mux.Vars(r)
	id := vars["id"]

	reqBody, _ := ioutil.ReadAll(r.Body)

	var updatedBook Book 
    json.Unmarshal(reqBody, &updatedBook)

    // TODO Add handling around JSON reading

    found := false

	for index, book := range books {
		if book.ID == id {
			books[index] = updatedBook
            found = true
            break
		}
	}

    if found == true {
        json.NewEncoder(w).Encode(updatedBook)
    } else {
        // handling when book not found
        http.Error(w, "Not Found", http.StatusNotFound)
    }
}