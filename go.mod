// +heroku install .

module gitlab.com/shuaiscott/library-go-api

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.8.0
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/tools v0.0.0-20200811032001-fd80f4dbb3ea // indirect
	google.golang.org/genproto v0.0.0-20200806141610-86f49bd18e98 // indirect
)
