package main

import (
	"database/sql"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"gitlab.com/shuaiscott/library-go-api/database"
	"gitlab.com/shuaiscott/library-go-api/v1"
	"log"
	"net/http"
	"os"
)

func main() {

	// initDB()

	handleRequests()
}

func initDB() {
	var err error
	database.DBCon, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))

	if err != nil {
		log.Fatalf("Error opening database: %q", err)
	}

	if err = database.DBCon.Ping(); err != nil {
		log.Panic(err)
	}
}

// func HelloWorld(w http.ResponseWriter, r *http.Request) {
// 	fmt.Fprintf(w, "Hello, you've requested: %s\n", r.URL.Path)
// }

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/", homePage)

	// Book List Retrieval
	myRouter.HandleFunc("/v1/books", v1.ReturnAllBooks)

	// CRUD
	myRouter.HandleFunc("/v1/book", v1.CreateBook).Methods("POST")            // Create
	myRouter.HandleFunc("/v1/books/{id}", v1.ReturnSingleBook).Methods("GET") // Read
	myRouter.HandleFunc("/v1/books/{id}", v1.UpdateBook).Methods("PUT")       // Update
	myRouter.HandleFunc("/v1/books/{id}", v1.DeleteBook).Methods("DELETE")    // Delete

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Printf("listening on port %v\n", port)

	log.Fatal(http.ListenAndServe(":"+port, myRouter))
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the homePage!")
	fmt.Println("Endpoint Hit: homePage")
}
