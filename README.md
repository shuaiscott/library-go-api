# Scott's Library Book CRUD API

## Endpoints
- Production - https://scott-library-api.herokuapp.com
- Staging - https://scott-library-api-staging.herokuapp.com

## Resources
- /v1/books (GET) - Return all books in the library (non-paginated)
- /v1/books/{id} (GET) - Return a single book by id
- /v1/book (POST) - Create a single book
- /v1/books/{id} (PUT) - Update a single book by id
- /v1/books/{id} (Delete) - Delete a single book by id

